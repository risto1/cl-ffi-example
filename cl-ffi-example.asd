(defsystem "cl-ffi-example"
  :class :package-inferred-system
  :author "Risto Stevcev"
  :licence "BSD-3-Clause"
  :depends-on ()
  :components ((:file "cl-ffi-example")))
