CC = gcc
LDFLAGS = -shared
SOURCE = mylib.c
OBJECT=$(SOURCE:.c=.o)
SOBJECT=lib$(SOURCE:.c=.so)
LIBS =
CFLAGS = -g -Wall -fPIC
LIBNAME = libmylib.a

all: $(OBJECT) $(SOBJECT) $(LIBNAME)

$(OBJECT): $(SOURCE)
	$(CC) $(CFLAGS) -o $@ -c $<

$(SOBJECT): $(OBJECT)
	$(CC) -shared $(OBJECT) -o $(SOBJECT)

$(LIBNAME): $(SOBJECT)
	ar rcs $(LIBNAME) $(OBJECT)

.PHONY: clean
clean:
	rm -rf $(OBJECT) $(SOBJECT)
