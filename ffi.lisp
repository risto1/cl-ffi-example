(defpackage ffi
  (:use :cl))

(in-package :ffi)

(require 'asdf)

(ffi:load-foreign-library (ffi:find-foreign-library '("mylib") (uiop:getcwd)))

(load "cl-ffi-example")
