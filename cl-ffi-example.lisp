(defpackage cl-ffi-example
  (:use :cl)
  (:export #:some-int #:*some-string* #:*some-struct* #:my-struct-int #:my-struct-string))

(in-package :cl-ffi-example)

(eval-when (:compile-toplevel)
  ;; Static library
  (setf c::*user-ld-flags* "libmylib.a")

  ;; Dynamic library
  #+nil(setf c::*user-ld-flags* (format nil "-lmylib -L~A -Wl,--rpath,~A"
                                        (directory-namestring *compile-file-pathname*)
                                        (directory-namestring *compile-file-pathname*))))

;;(eval-when (:compile-toplevel :load-toplevel))

;;(ffi:load-foreign-library "/home/risto/projects/cl-ffi-example/mylib.so")

(ffi:def-function ("some_int" some-int) () :returning :int)

#+nil(some-int) ; 123


(ffi:def-foreign-var ("some_string" *some-string*) :cstring :default)

#+nil(progn *some-string*) ; "Hello, World!"


(ffi:def-struct my-struct (my-string :cstring) (my-int :int))

(ffi:def-foreign-var ("some_struct" *some-struct*) my-struct :default)

#+nil(ffi:get-slot-value *some-struct* 'my-struct 'my-int) ; 1234
#+nil(ffi:get-slot-value *some-struct* 'my-struct 'my-string) ; "Hello"


(ffi:def-function ("my_struct_int" my-struct-int) ((s (* my-struct))) :returning :int)

#+nil(my-struct-int *some-struct*) ; 1234


(ffi:def-function ("my_struct_string" my-struct-string) ((s (* my-struct)))
  :returning :cstring)

#+nil(my-struct-string *some-struct*) ; "Hello"
